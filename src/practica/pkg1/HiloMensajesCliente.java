package practica.pkg1;

import practica.pkg1.VCliente;
import static practica.pkg1.VCliente.AreaM;
import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HiloMensajesCliente extends Thread {
    Socket clientSocket;
    PrintWriter out;
    BufferedReader in;              
    String buffer;
    SimpleDateFormat formatterMDY;
    boolean salir = false;
    String ip;
   
    
    public HiloMensajesCliente(String IP){
        ip=IP;
      
    }
    
    public void run(){        
        String fechaHora;
        VCliente.AreaM.setText("Iniciando Chat");
        formatterMDY = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            
        try {
            clientSocket = new Socket(ip,3333);

            out =
                    new PrintWriter(clientSocket.getOutputStream(), true);
            
            in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));  
            
            while(!this.salir){                
                buffer = in.readLine();
                
                if (buffer!=null){
                    fechaHora = formatterMDY.format(Calendar.getInstance().getTime());
                    VCliente.AreaM.setText(AreaM.getText()+"\n"+fechaHora+ " < "+buffer);
                }                
            }                        
        } catch (IOException ex) {
            Logger.getLogger(HiloMensajesCliente.class.getName()).log(Level.SEVERE, null, ex);
        }                           
    }

    void enviar(String mensaje) {
        out.println(mensaje);
    }

    void cerrar() {
        try {
            clientSocket.close();
            in.close();
            out.close();
            this.salir = true;
        } catch (IOException ex) {
            Logger.getLogger(HiloMensajesCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
